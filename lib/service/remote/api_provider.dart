import 'package:dio/dio.dart';
import 'package:flutter_employee/data/data.dart';
import 'package:flutter_employee/service/exception/network_exceptions/network_exceptions.dart';
import 'package:flutter_employee/service/exception/sentry_exception/sentry_exception.dart';
import 'package:flutter_employee/utils/utils.dart';

class ApiProvider {
  Dio dio;
  final SentryException sentryException = SentryException();

  ApiProvider() {
    BaseOptions options = BaseOptions(
      baseUrl: 'https://randomuser.me/api',
      connectTimeout: 15000,
      receiveTimeout: 15000,
    );
    dio = Dio(options);
    dio.interceptors.clear();
    dio.interceptors.add(LoggingInterceptor(dio));
  }

  // ? Get list of users
  Future<UserResponse> getUserList(String number) async {
    try {
      final Response response = await dio.get('/?results=$number');
      return UserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      // sentryException.reportErrorEvent(error, stacktrace, '/?results=$number');
      return UserResponse.withError(NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(error)));
    }
  }
}
