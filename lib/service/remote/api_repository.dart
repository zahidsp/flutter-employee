import 'package:flutter_employee/data/data.dart';
import 'package:flutter_employee/service/remote/api_provider.dart';

class ApiRepository {
  ApiProvider _apiProvider = ApiProvider();

  // ? User
  Future<UserResponse> usersListData(String number) =>
      _apiProvider.getUserList(number);
}
