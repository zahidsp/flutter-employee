import 'dart:async';
import 'dart:ui' as ui show window;

import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info/package_info.dart';
import 'package:sentry/sentry.dart';

class SentryException {
  // TODO 'Define Sentry dsn'
  final SentryClient sentry = new SentryClient(dsn: 'dsn sentry');

  Future<void> reportError(dynamic error, dynamic stacktrace) async {
    print('Reporting to Sentry.io...');

    final SentryResponse response =
        await sentry.captureException(exception: error, stackTrace: stacktrace);

    if (response.isSuccessful) {
      print('Success! Event ID: ${response.eventId}');
    } else {
      print('Failed to report to Sentry.io: ${response.error}');
    }
  }

  Future<void> reportErrorEvent(
      dynamic error, dynamic stacktrace, String url) async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    Map<String, dynamic> extra = {};
    final PackageInfo info = await PackageInfo.fromPlatform();

    if (defaultTargetPlatform == TargetPlatform.android) {
      extra['device_info'] =
          _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      extra['device_info'] = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
    }

    extra['url'] = url;

    Map<String, String> tags = {};
    tags['platform'] =
        defaultTargetPlatform.toString().substring('TargetPlatform.'.length);
    tags['package_name'] = info.packageName;
    tags['build_number'] = info.buildNumber;
    tags['version'] = info.version;
    tags['locale'] = ui.window.locale.toString();

    final Event event = Event(
      loggerName: '',
      exception: error,
      stackTrace: stacktrace,
      release: '${info.version}_${info.buildNumber}',
      tags: tags,
      extra: extra,
    );

    await sentry.capture(event: event);
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.release': build.version.release,
      'version.codename': build.version.codename,
      'board': build.board,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }
}
