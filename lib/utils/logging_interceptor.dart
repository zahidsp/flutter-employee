import 'package:dio/dio.dart';

class LoggingInterceptor extends InterceptorsWrapper {
  final Dio _dio;

  LoggingInterceptor(this._dio);

  @override
  Future onRequest(RequestOptions options) async {
    print(
        "--> ${options.method != null ? options.method.toUpperCase() : 'METHOD'} ${"" + (options.baseUrl ?? "") + (options.path ?? "")}");
    print("Headers:");
    options.headers.forEach((k, v) => print('$k: $v'));
    if (options.queryParameters != null) {
      print("queryParameters:");
      options.queryParameters.forEach((k, v) => print('$k: $v'));
    }
    if (options.data != null) {
      print("Body: ${options.data}");
    }
    print(
        "--> END ${options.method != null ? options.method.toUpperCase() : 'METHOD'}");

    return options;
  }

  @override
  Future onResponse(Response response) {
    print(
        "<-- ${response.statusCode} ${(response.request != null ? (response.request.baseUrl + response.request.path) : 'URL')}");
    print("Headers:");
    response.headers?.forEach((k, v) => print('$k: $v'));
    print("Response: ${response.data}");
    print("<-- END HTTP");
    return super.onResponse(response);
  }

  @override
  Future onError(DioError dioError) async {
    print(
        "<-- ${dioError.message} ${(dioError.response?.request != null ? (dioError.response.request.baseUrl + dioError.response.request.path) : 'URL')}");
    print(
        "${dioError.response != null ? dioError.response.data : 'Unknown Error'}");
    print("<-- End error");

// String oldAccessToken = _sharedPreferencesManager
    //     .getString(SharedPreferencesManager.keyAccessToken);
    int responseCode = dioError.response.statusCode;

    if (responseCode == 401) {
      print('MSG FROM INTERCERPTOR !! 401 TOKEN EXPIRED');
      _dio.interceptors.requestLock.lock();
      _dio.interceptors.responseLock.lock();
    } else {
      super.onError(dioError);
    }

    // if (oldAccessToken != null &&
    //     responseCode == 401 &&
    //     _sharedPreferencesManager != null) {
    //   navigationService.navigateTo('/login',
    //       arguments: (Route<dynamic> route) => false);
    //   locator<SharedPreferencesManager>().clearAll();

    //   _dio.interceptors.requestLock.lock();
    //   _dio.interceptors.responseLock.lock();

    //   RequestOptions options = dioError.response.request;
    //   options.headers.addAll({'requirestoken': true});
    //   _dio.interceptors.requestLock.unlock();
    //   _dio.interceptors.responseLock.unlock();
    //   return _dio.request(options.path, options: options);
    // } else {
    //   super.onError(dioError);
    // }
  }
}
