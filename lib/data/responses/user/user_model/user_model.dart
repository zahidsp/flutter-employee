import 'package:flutter_employee/data/responses/user/user_detail/user_detail.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  String gender;
  Name name;
  Location location;
  String email;
  String phone;
  Picture picture;

  UserModel(
      {this.gender,
      this.email,
      this.phone,
      this.name,
      this.location,
      this.picture});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
