import 'package:flutter_employee/data/responses/user/user_model/user_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_response.g.dart';

@JsonSerializable()
class UserResponse {
  List<UserModel> results;
  @JsonKey(ignore: true)
  String error;

  UserResponse({this.results});
  UserResponse.withError(this.error);

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
}
