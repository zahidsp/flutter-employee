import 'package:flutter_employee/presentation/screens/screens.dart';
import 'package:get/get.dart';

List<GetPage> routes = [
  GetPage(name: HomeScreen.route, page: () => HomeScreen()),
  GetPage(
      name: DetailScreen.route,
      page: () => DetailScreen(userModel: Get.arguments))
];
