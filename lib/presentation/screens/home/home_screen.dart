part of '../screens.dart';

class HomeScreen extends HookWidget {
  static const String route = '/';

  @override
  Widget build(BuildContext context) {
    useEffect(() {
      userListBloc.getUserList('15');
      return;
    }, const []);

    Future onRefresh() async {
      userListBloc.getUserList('15');
    }

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarCustom(
              implyLeading: false,
              title: 'FlutterEmployees',
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: onRefresh,
                child: StreamBuilder(
                  stream: userListBloc.userListData,
                  builder: (BuildContext context,
                      AsyncSnapshot<UserResponse> snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data.error != null &&
                          snapshot.data.error.length > 0) {
                        print('ada error: ${snapshot.data.error}');
                        return FailedScreen(
                          errValue: snapshot.data.error,
                          retry: onRefresh,
                        );
                      }

                      List<UserModel> userResponse = snapshot.data.results;

                      return ListView(
                        padding:
                            EdgeInsets.symmetric(horizontal: defaultPadding),
                        children: [
                          BestList(
                            userResponse: userResponse,
                          ),
                          AllList(
                            userResponse: userResponse,
                          )
                        ],
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
