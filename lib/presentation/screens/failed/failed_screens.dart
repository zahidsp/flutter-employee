part of '../screens.dart';

class FailedScreen extends HookWidget {
  final String errValue;
  final Function retry;

  FailedScreen({this.errValue, this.retry});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 18),
          // height: 320,
          // color: Colors.yellowAccent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: 160,
                child: Image.asset(
                  'assets/people_nodata.png',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 24),
              Text(
                'Oopps!!!',
                style: blackFontStyle1.copyWith(color: Colors.redAccent),
              ),
              SizedBox(height: 8),
              Text(
                errValue,
                textAlign: TextAlign.center,
                style: bodyFontStyle1.copyWith(height: 1.5),
              ),
              SizedBox(height: 40),
              SizedBox(
                  width: 120,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                        side: BorderSide(color: mainColor)),
                    child: Text('RETRY',
                        style: blackFontStyle2.copyWith(color: mainColor)),
                    onPressed: retry,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
