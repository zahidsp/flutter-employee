part of '../screens.dart';

class DetailScreen extends HookWidget {
  static const String route = 'detail';

  final UserModel userModel;

  DetailScreen({this.userModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [
        AppBarCustom(
          implyLeading: true,
          title: 'Details',
        ),
        TopSectionDetail(
          urlImage: userModel.picture.large,
          name: userModel.name.title +
              ' ' +
              userModel.name.first +
              ' ' +
              userModel.name.last,
          email: userModel.email,
        ),
        InfoTile(
            iconData: CupertinoIcons.person,
            label: 'Gender',
            body: userModel.gender),
        InfoTile(
            iconData: CupertinoIcons.phone,
            label: 'Phone',
            body: userModel.phone),
        InfoTile(
            iconData: CupertinoIcons.location,
            label: 'Location',
            body: userModel.location.city + ', ' + userModel.location.state),
      ],
    ));
  }
}
