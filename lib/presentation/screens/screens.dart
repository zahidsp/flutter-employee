import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_employee/bloc/user_list_bloc/user_list_bloc.dart';
import 'package:flutter_employee/data/data.dart';
import 'package:flutter_employee/presentation/widget/widget.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_employee/theme/theme.dart';

part './home/home_screen.dart';
part './detail/detail_screen.dart';
part './failed/failed_screens.dart';
