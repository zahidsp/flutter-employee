part of 'widget.dart';

class UserFavCard extends HookWidget {
  final UserModel singleUser;
  final LinearGradient gradient;
  final String rank;

  UserFavCard({this.singleUser, this.gradient, this.rank});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(DetailScreen.route, arguments: singleUser);
      },
      child: Container(
        width: 160,
        padding: EdgeInsets.all(defaultMedPadding),
        decoration: BoxDecoration(
            gradient: gradient,
            borderRadius: BorderRadius.circular(24),
            boxShadow: [
              BoxShadow(
                spreadRadius: 2,
                blurRadius: 12,
                color: Colors.black12,
                offset: Offset(2, 6),
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          children: [
            CircleAvatar(
              radius: 44,
              child: ClipOval(
                child: Image.network(
                  singleUser.picture.large,
                ),
              ),
            ),
            SizedBox(width: 16),
            Text('#' + rank,
                style: blackFontStyle1.copyWith(
                    fontSize: 32, color: Colors.white)),
            SizedBox(width: 16),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  singleUser.name.first + ' ' + singleUser.name.last,
                  style: blackFontStyle2.copyWith(
                      fontWeight: FontWeight.w600, color: Colors.white),
                  maxLines: 2,
                ),
                // SizedBox(height: 4),
                // SizedBox(
                //   width: 140,
                //   child: Text(
                //     singleUser.location.city + ', ' + singleUser.location.state,
                //     style: bodyFontStyle2.copyWith(color: Colors.white),
                //     maxLines: 2,
                //     textAlign: TextAlign.center,
                //   ),
                // )
              ],
            )
          ],
        ),
      ),
    );
  }
}
