part of 'widget.dart';

class UserCard extends HookWidget {
  final UserModel singleUser;

  UserCard({this.singleUser});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(DetailScreen.route, arguments: singleUser);
      },
      child: Container(
        padding: EdgeInsets.all(defaultMedPadding),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(spreadRadius: 2, blurRadius: 15, color: Colors.black12)
            ]),
        child: Row(
          children: [
            CircleAvatar(
              radius: 24,
              child: ClipOval(
                child: Image.network(
                  singleUser.picture.thumbnail,
                ),
              ),
            ),
            SizedBox(width: 16),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    singleUser.name.first + ' ' + singleUser.name.last,
                    style: blackFontStyle2.copyWith(
                        fontWeight: FontWeight.w700, color: mainColor),
                    overflow: TextOverflow.ellipsis,
                  ),
                  // SizedBox(height: 4),
                  Text(
                    singleUser.location.city + ', ' + singleUser.location.state,
                    style: bodyFontStyle2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
