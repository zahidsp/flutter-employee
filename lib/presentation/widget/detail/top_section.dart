part of '../widget.dart';

class TopSectionDetail extends HookWidget {
  final String urlImage;
  final String name;
  final String email;

  TopSectionDetail({this.urlImage, this.name, this.email});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: defaultPadding),
        height: 220,
        margin: EdgeInsets.only(bottom: defaultPadding),
        width: double.infinity,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 110,
              height: 110,
              margin: EdgeInsets.only(bottom: 16),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/photo_border.png'))),
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(urlImage), fit: BoxFit.cover)),
              ),
            ),
            Text(name,
                style: blackFontStyle1.copyWith(
                    fontWeight: FontWeight.w700, color: mainColor)),
            SizedBox(height: 4),
            Text(email, style: blackFontStyle2)
          ],
        ));
  }
}
