part of '../widget.dart';

class InfoTile extends HookWidget {
  final IconData iconData;
  final String label;
  final String body;

  InfoTile({this.iconData, this.label, this.body});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          bottom: defaultPadding, left: defaultPadding, right: defaultPadding),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
              height: 24,
              width: 24,
              child: Icon(
                iconData,
                color: greyColor,
              )),
          SizedBox(width: 16),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                label,
                style: bodyFontStyle2,
              ),
              Text(
                body,
                style: blackFontStyle2,
              )
            ],
          )
        ],
      ),
    );
  }
}
