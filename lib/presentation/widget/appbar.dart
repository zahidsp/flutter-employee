part of 'widget.dart';

class AppBarCustom extends HookWidget {
  final String title;
  final bool implyLeading;

  AppBarCustom({this.title, this.implyLeading});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: defaultPadding),
        width: double.infinity,
        height: 64,
        child: Row(children: [
          implyLeading
              ? Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      color: greyColor.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(40)),
                  child: InkWell(
                    onTap: () => implyLeading ? Get.back() : null,
                    borderRadius: BorderRadius.circular(40),
                    child: Icon(
                      CupertinoIcons.chevron_back,
                      color: Colors.black,
                      size: 24,
                    ),
                  ),
                )
              : SizedBox(),
          implyLeading ? SizedBox(width: 16) : SizedBox(),
          Text(
            title,
            style: blackFontStyle1.copyWith(fontWeight: FontWeight.w700),
          ),
        ]));
  }
}
