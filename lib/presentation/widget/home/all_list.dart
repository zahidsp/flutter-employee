part of '../widget.dart';

class AllList extends HookWidget {
  final List<UserModel> userResponse;

  AllList({this.userResponse});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MenuTitle(title: 'Our Happy Employees'),
        Column(
          children: userResponse
              .map((e) => Padding(
                  padding: EdgeInsets.only(
                      // top: (e == userResponse.first)
                      //     ? defaultMedPadding
                      //     : 0,
                      bottom: defaultMedPadding),
                  child: UserCard(singleUser: e)))
              .toList(),
        ),
      ],
    );
  }
}
