part of '../widget.dart';

class BestList extends HookWidget {
  final List<UserModel> userResponse;

  BestList({this.userResponse});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MenuTitle(title: 'Our Best Employees'),
        Container(
          height: 258,
          width: double.infinity,
          child: ListView.builder(
            padding: EdgeInsets.only(
                left: defaultMedPadding, bottom: defaultPadding),
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (BuildContext context, int i) {
              UserModel singleUser = userResponse[i];
              List<LinearGradient> gradients = listGradient;
              int rank = i + 1;
              return Padding(
                padding: const EdgeInsets.only(
                  right: defaultMedPadding,
                ),
                child: UserFavCard(
                    singleUser: singleUser,
                    gradient: gradients[i],
                    rank: rank.toString()),
              );
            },
          ),
        ),
      ],
    );
  }
}
