part of 'widget.dart';

class MenuTitle extends HookWidget {
  final String title;

  MenuTitle({this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: defaultPadding),
      child: Text(
        title,
        style: blackFontStyle1,
      ),
    );
  }
}
