import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_employee/data/data.dart';
import 'package:flutter_employee/presentation/screens/screens.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_employee/theme/theme.dart';
import 'package:get/get.dart';

part 'user_card.dart';
part 'user_fav_card.dart';
part 'menu_title.dart';
part 'appbar.dart';

// ? HOME WIDGET
part './home/best_list.dart';
part './home/all_list.dart';

// ? DETAIL WIDGET
part './detail/info_tile.dart';
part './detail/top_section.dart';
