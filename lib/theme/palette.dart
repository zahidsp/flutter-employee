part of 'theme.dart';

// ? SOLID COLOR

Color mainColor = "666CF5".toColor();
Color greyColor = "8D92A3".toColor();
Color lightPurpleColor = "DB6AF4".toColor();
Color purpleColor = "AB4BF1".toColor();
Color lightBlueColor = "82C6F1".toColor();
Color blueColor = "7DA1F3".toColor();
Color lightTealColor = "72E7CF".toColor();
Color tealColor = "56B08F".toColor();

// ? GRADIENT COLOR

List<LinearGradient> listGradient = [
  gradientPurple,
  gradientBlue,
  gradientTeal
];

LinearGradient gradientPurple = LinearGradient(
  begin: Alignment.bottomRight,
  end: Alignment.topLeft,
  colors: [purpleColor, lightPurpleColor],
  stops: [
    0.2,
    2,
  ],
);

LinearGradient gradientBlue = LinearGradient(
  begin: Alignment.bottomRight,
  end: Alignment.topLeft,
  colors: [blueColor, lightBlueColor],
  stops: [
    0.2,
    2,
  ],
);

LinearGradient gradientTeal = LinearGradient(
  begin: Alignment.bottomRight,
  end: Alignment.topLeft,
  colors: [tealColor, lightTealColor],
  stops: [
    0.2,
    2,
  ],
);
