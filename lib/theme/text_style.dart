part of 'theme.dart';

TextStyle blackFontStyle1 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500);
TextStyle blackFontStyle2 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500);
TextStyle bodyFontStyle1 = GoogleFonts.poppins().copyWith(color: Colors.black);
TextStyle bodyFontStyle2 = GoogleFonts.poppins().copyWith(color: greyColor);
