part of 'theme.dart';

const double defaultPadding = 20;
const double defaultMedPadding = 16;
const double defaultMiniPadding = 12;
