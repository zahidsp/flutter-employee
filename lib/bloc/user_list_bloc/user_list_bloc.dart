import 'package:flutter_employee/data/data.dart';
import 'package:flutter_employee/service/remote/api_repository.dart';
import 'package:rxdart/rxdart.dart';

class UserListBloc {
  final ApiRepository _apiRepository = ApiRepository();
  final BehaviorSubject<UserResponse> _subject =
      BehaviorSubject<UserResponse>();

  getUserList(String number) async {
    _subject.value = null;
    UserResponse userResponse = await _apiRepository.usersListData(number);
    _subject.sink.add(userResponse);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<UserResponse> get userListData => _subject;
}

final userListBloc = UserListBloc();
